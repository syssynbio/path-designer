# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import uuid
from typing import Dict, List, TextIO

import pandas as pd
import pydot

import cobra
from sympy.core.expr import Expr
from optlang import symbolics
from sbtab.SBtab import SBtabTable


class Solution(object):
    """Contained class for path solutions."""

    def __init__(
            self,
            path: object,
            objective_value: float,
            active_indicators: List[str],
            fluxes: pd.Series
    ):
        """

        :param path: a Path object, used to get the COBRA model and the
        cofactor metabolites
        :param objective_value: the number of active reactions (that have
        indicators)
        :param active_reactions: the irreversible reaction IDs with non-zero
        fluxes
        :param fluxes: a Series whose index are COBRA Reactions and the
        values are the fluxes in the solution
        """
        self.cobra_model = path.cobra_model
        self.cofactor_set = path.cofactor_set
        self.objective_value = objective_value
        self.active_indicators = active_indicators
        self.fluxes = fluxes

    @property
    def total_flux(self) -> float:
        """The sum of all active fluxes"""
        return sum([abs(flux) for rxn_id, flux in self.fluxes.items()
                    if rxn_id[0:2] != "__"])

    @property
    def sum_of_gammas(self) -> Expr:
        """Return an expression for the sum of all active indicators."""
        return symbolics.add(self.active_indicators)

    def write_to_file(
            self,
            handle: TextIO,
            counter: int
    ) -> None:
        """Write the solution to a KEGG-style file"""

        handle.write(f'ENTRY       M-PATHWAY_{counter:03d}\n')
        handle.write(f'SKIP        FALSE\n')
        handle.write(f'NAME        M-PATHWAY_{counter:03d}\n')
        handle.write(f'TYPE        MARGIN\n')
        handle.write(f'CONDITIONS  pH=7.0,I=0.0,T=300\n')
        handle.write(f'C_MID       0.0001\n')

        first = True
        for rxn_id, flux in self.fluxes.items():
            if rxn_id[0:2] == "__":
                continue
            rxn = self.cobra_model.reactions.get_by_id(rxn_id)
            if first:
                prefix = "REACTION"
                first = False
            else:
                prefix = ""
            handle.write(f"{prefix:12s}{rxn} (x{flux:.3f})\n")

        handle.write('///\n')
        handle.flush()

    def to_sbtab(self, counter: int) -> None:
        """Write the solution to a KEGG-style file"""
        reaction_data = []
        for rxn_id, flux in self.fluxes.items():
            if rxn_id[0:2] == "__":
                continue
            rxn = self.cobra_model.reactions.get_by_id(rxn_id)
            reaction_data.append((rxn_id, rxn.build_reaction_string(), flux))

        reaction_df = pd.DataFrame(data=reaction_data,
                                   columns=["id", "formula", "flux"])

        return SBtabTable.from_data_frame(
            reaction_df,
            table_id=f"Pathway_{counter:03d}",
            table_type="Reaction"
        )

    def create_metabolite_node(self, met: cobra.Metabolite) -> pydot.Node:
        """Create a graphviz Node for a Metabolite.

        :param met: A Metabolite
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{met.id}"')
        node.set("tooltip", met.name)
        node.set("fontsize", "12")
        node.set("fontname", "verdana")
        node.set("style", "filled")

        url = (f"\"http://bigg.ucsd.edu/universal/metabolites/"
               f"{met.id.rsplit('_', 1)[0]}\"")
        if met.id[0:2] == "__":
            node.set("shape", "tripleoctagon")
            node.set("fontcolor", "darkgreen")
            node.set("fillcolor", "white")
        elif met in self.cofactor_set:
            node.set("shape", "plaintext")
            node.set("URL", url)
            node.set("fontcolor", "dodgerblue") # color for cofactors
            node.set("fillcolor", "white")
        else:
            node.set("shape", "octagon")
            node.set("URL", url)
            node.set("fontcolor", "white") # color for non-cofcators
            node.set("fillcolor", "dodgerblue")
        return node

    @staticmethod
    def create_reaction_node(rxn: cobra.Reaction) -> pydot.Node:
        """Create a graphviz node for a Reaction.

        :param rxn: a Reaction
        :return: a pydot.Node object
        """
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set("label", f'"{rxn.id}"')
        node.set("tooltip", rxn.id)
        node.set("URL", f'"http://bigg.ucsd.edu/universal/reactions/{rxn.id}"')
        node.set("shape", "oval")
        node.set("style", "filled")
        node.set("fontcolor", "darkgreen")
        node.set("fillcolor", "white")
        node.set("fontsize", "7")
        node.set("fontname", "verdana")
        return node

    @staticmethod
    def create_edge(
            m_node: pydot.Node,
            r_node: pydot.Node,
            coefficient: float = 1.0
    ) -> pydot.Edge:
        """Create a graphviz edge between a metabolite and a reaction.

        :param m_node: A Metabolite Node
        :param r_node: A Reaction Mode
        :param coefficient: The stoichiometric coefficient of this pair
        :return: A pydot.Edge object
        """

        if coefficient < 0:
            edge = pydot.Edge(m_node, r_node)
        else:
            edge = pydot.Edge(r_node, m_node)

        if abs(abs(coefficient) - 1.0) > 1e-3:
            edge.set("label", f'"{abs(coefficient):.3g}"')

        edge.set("arrowhead", "open")
        edge.set("arrowtail", "none")
        edge.set("color", "cadetblue")  # edge line color
        edge.set("fontcolor", "indigo")  # edge label color
        edge.set("fontname", "verdana")
        edge.set("fontsize", "10")
        return edge

    def draw_graph(self) -> pydot.Dot:
        """Draw the solution using graphviz"""
        g_dot = pydot.Dot()

        r_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of reaction nodes
        m_nodes: Dict[cobra.Reaction, pydot.Node] = {}  # dict of metabolite
        # nodes

        for rxn_id, flux in self.fluxes.items():
            if rxn_id[0:2] == "__":
                continue
            rxn = self.cobra_model.reactions.get_by_id(rxn_id)
            node = self.create_reaction_node(rxn)
            r_nodes[rxn] = node
            g_dot.add_node(node)

            for met in rxn.metabolites:
                if met in self.cofactor_set:
                    # make a special node of this co-factor for each reaction
                    # that uses it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                elif met not in m_nodes:
                    # This is a normal metabolite, create only one node
                    # for it
                    m_node = self.create_metabolite_node(met)
                    g_dot.add_node(m_node)
                    m_nodes[met] = m_node
                else:
                    m_node = m_nodes[met]

                edge = self.create_edge(
                    m_node, r_nodes[rxn],
                    rxn.get_coefficient(met) * flux
                )
                g_dot.add_edge(edge)
        return g_dot
