import re
import csv
import pydot
import logging
from typing import Tuple, Dict, Iterable
import pandas as pd
from os import path
import uuid
from . import KEGG_PATH

################################################################################
#                               EXCEPTIONS                                     #
################################################################################


class KeggParseException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class KeggNonCompoundException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class KeggReactionNotBalancedException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Reaction(object):
    def __init__(self, name, rid, sparse_reaction, weight=1):
        self.name = name
        self.rid = rid
        self.sparse_reaction = sparse_reaction
        self.weight = weight

    def __str__(self) -> str:
        return self.name

    def __cmp__(self, other) -> bool:
        return self.name.cmp(other.name)
    
    def get_cids(self) -> Iterable[str]:
        return self.sparse_reaction.keys()
    
    def unique_string(self):
        return " + ".join(
            [("%g %s") % (coeff, cid)
             for cid, coeff in sorted(self.sparse_reaction.items())])
    
    def is_futile(self):
        return max([abs(x) for x in self.sparse_reaction.values()]) > 1e-3
    
    def is_balanced(self, cid2atom_bag):
        atom_diff = {}
        for cid, coeff in self.sparse_reaction.items():
            atom_bag = cid2atom_bag.get(cid, {})
            if atom_bag is None:
                # this reaction cannot be checked since there is an
                # unspecific compound
                return False
            for atomic_number, atom_count in atom_bag.items():
                atom_diff[atomic_number] = (
                        atom_diff.get(atomic_number, 0) + coeff * atom_count
                )

        # ignore H and O inconsistencies
        atom_diff[1] = 0
        atom_diff[8] = 0

        return max([abs(x) for x in atom_diff.values()]) < 1e-3

    @staticmethod
    def parse_reaction_formula_side(formula: str) -> Dict[str, float]:
        """parse the side formula, e.g. '2 C00001 + C00002 + 3 C00003'."""
        compound_bag = {}
        for member in re.split(r"\s+\+\s+", formula):
            tokens = member.split(None, 1)
            if len(tokens) == 1:
                amount = 1
                key = member
            else:
                try:
                    amount = int(tokens[0])
                except ValueError:
                    raise KeggParseException("Non-specific reaction: " + formula)
                key = tokens[1]

            if key[0] != 'C':
                raise KeggNonCompoundException(
                    "Compound ID doesn't start with C: " + key)
            try:
                cid = key
                compound_bag[cid] = compound_bag.get(cid, 0.0) + amount
            except ValueError:
                raise KeggParseException("Non-specific reaction: " + formula)

        return compound_bag

    @staticmethod
    def parse_reaction_formula(formula):
        """ parse a two-sided formula such as: 2 C00001 => C00002 + C00003
            return the set of substrates, products and the direction of the reaction
        """
        tokens = re.split("([^=^<]+) (<*=>*) ([^=^>]+)", formula)
        if len(tokens) != 5:
            return None
        left_bag = Reaction.parse_reaction_formula_side(tokens[1])
        direction = tokens[2]  # the direction: <=, => or <=>
        right_bag = Reaction.parse_reaction_formula_side(tokens[3])
        return left_bag, right_bag, direction

    @staticmethod
    def formula_to_atombag(formula):
        """
            Given a string representing a chemical formula, returns a bag
            containing the atoms in the formula
        """
        if formula == "?" or formula.find("(") != -1 or formula.find(")") != -1:
            return {}

        atom_bag = {}
        for atom, count in re.findall("([A-Z][a-z]*)([0-9]*)", formula):
            if count == '':
                count = 1
            else:
                count = int(count)
            atom_bag[atom] = atom_bag.get(atom, 0) + count

        if "R" in atom_bag:
            # this formula is not full ('R' is a wildcard not an atom)
            return {}

        return atom_bag


class Kegg(object):

    COMPOUND_FILE = path.join(KEGG_PATH, "compound.txt")
    REACTION_FILE = path.join(KEGG_PATH, "reaction.txt")
    INCHI_FILE = path.join(KEGG_PATH, "inchi.txt")

    def __init__(self) -> object:

        self.font = "verdana"

        # cid2uid is a used for two purposes. One is to have canonical IDs for compounds
        # according to their INCHI labels (i.e. if two CIDs have the same INCHI, all occurences of the second
        # one will be changed to the first appearing CID). If a CID is not in the INCHI file, but is used
        # by one of the reactions, it might cause the reaction to be skipped. If compound formulas are to be
        # added using "database_updates.txt", the SETC line should appear at the top.
        self.cid2uid_map = {}
        self.compound2names_map = {}
        self.compound2atoms_map = {} # each value is a map of atoms to counts
        self.banned_compounds = set() # if one of these CIDs participates in a reaction, skip it
        self.banned_reactions = set() # skip all the of the reactions with these RIDs 

        # being in cofactor_set only changes how a metabolite is depicted in the
        # graphviz output
        self.cofactor_set = set()

        # being in ignored_set means that this co-factor is ignored both for
        # the stoichiometric mass balancing and in the graphviz output (e.g. H+)
        self.ignored_set = set()
        
        self.reactions = []

    def prepare_all(self, UPDATE_FILE):
        self.parse_database()    
        self.update_database(UPDATE_FILE)
        self.preprocess_database()
    
    def parse_database(self):
        self.parse_compound_file()
        self.parse_inchi_file()
        self.parse_reaction_file()
   
    def parse_compound_file(self):
        compound_file = open(Kegg.COMPOUND_FILE, 'rt')
        for line in compound_file.readlines():
            if line.find('%') != -1:
                line = line[0:line.find('%')]
            cid, remainder = line.rstrip().split(': ')
            name, formula = remainder.split(' = ')
    
            atom_bag = Reaction.formula_to_atombag(formula)
            if len(atom_bag) > 0:
                self.compound2names_map[cid] = name.strip()
                self.compound2atoms_map[cid] = atom_bag
        compound_file.close()

    def formula_to_sparse(
            self,
            formula: str
    ) -> Tuple[Dict[str, float], str]:
        """
            translates a formula to a sparse-reaction
        """
        left_bag, right_bag, direction = \
            Reaction.parse_reaction_formula(formula)
        
        sparse_reaction = {}
        for cid, count in left_bag.items():
            sparse_reaction[cid] = sparse_reaction.get(cid, 0) - count 
        for cid, count in right_bag.items():
            sparse_reaction[cid] = sparse_reaction.get(cid, 0) + count 

        return sparse_reaction, direction
    
    def parse_inchi_file(self):
        """
            Parses the INCHI file and creates the cid2uid map for removing
            duplicates (CIDs of the same compound).
        """
        inchi_file = csv.reader(open(Kegg.INCHI_FILE, 'rt'), delimiter='\t')
        inchi2uid_map = {}
        for row in inchi_file:
            if len(row) != 2:
                continue
            cid, inchi = row
            if not inchi in inchi2uid_map:
                inchi2uid_map[inchi] = cid
            self.cid2uid_map[cid] = inchi2uid_map[inchi]

    def parse_reaction_file(
            self,
            reaction_fname: str = None
    ) -> None:
        """ Parse the formulas in reaction2formula_map and create a new map of reaction_id to pairs of
            substrates/products, taking the direction of each reaction into account.
            Note that two different reactions can have the same substrate-product pair, which means that
            there is redundancy in the database.
        """
        if reaction_fname is None:
            reaction_fname = Kegg.REACTION_FILE

        for cid in self.compound2names_map.keys():
            if not cid in self.cid2uid_map:
                self.cid2uid_map[cid] = cid
        reaction_file = open(reaction_fname, 'rt')
        for line in reaction_file.readlines():
            line = line.strip()
            if line.find('%') != -1:
                line = line[0:line.find('%')]
            if line == "":
                continue
                
            if len(line.split(':')) == 2:
                reaction_id, formula = line.split(':')
                rid = reaction_id.strip()
            else:
                raise Exception("Syntax Error - " + line)
    
            try:
                (sparse_reaction, direction) = self.formula_to_sparse(formula.strip())
                self.add_reaction(rid, direction, sparse_reaction)
            except KeggParseException:
                continue
            except KeggNonCompoundException:
                continue

    def update_database(self, fname):
        """
            Updates the database of reactions and compounds, using the update_database file.
            Commands are: SETR, NEWR, CARB, TRNS, DELR, SETC, NEWC, DELC, COFR
            
            In stiochio_mode, all reactions are not touched, so only SETC, NEWC, DELC, COFR are used.
        """
        
        update_file = open(fname, 'r')

        for line in update_file.readlines():
            if line.find('%') != -1:
                line = line[0:line.find('%')]
            line = line.strip()
            if line == '':
                continue
            command, rest = line.split(' ', 1)
            line = rest.strip()
            
            if command in ['SETR', 'NEWR', 'CARB', 'TRNS']:
                reaction_id, formula = line.split(':')
                rid = reaction_id.strip()
                try:
                    sparse_reaction, direction = \
                        self.formula_to_sparse(formula.strip())
                except KeggParseException:
                    continue
                self.add_reaction(rid, direction, sparse_reaction,
                                  name=reaction_id)
            elif command == 'DELR':
                rid = line
                self.banned_reactions.add(rid)
            elif command in ['SETC', 'NEWC']:
                cid, remainder = line.rstrip().split(': ')
                cid = cid.strip()
                if cid in self.cid2uid_map:
                    cid = self.cid2uid_map[cid]
                else:
                    # if this CID is not in the INCHI file
                    self.cid2uid_map[cid] = cid
                name, formula = remainder.split(' = ')
                atom_bag = Reaction.formula_to_atombag(formula)
                if len(atom_bag) == 0:
                    raise KeggParseException(
                        "SETC must have a valid formula for the compound: "
                        + line)
                self.compound2names_map[cid] = name
                self.compound2atoms_map[cid] = atom_bag
            elif command == 'DELC':
                cid = line
                if cid in self.cid2uid_map:
                    cid = self.cid2uid_map[cid]
                else:
                    # if this CID is not in the INCHI file
                    self.cid2uid_map[cid] = cid
                self.banned_compounds.add(cid)
            elif command == 'COFR':  # cofactor
                cid, name = line.split('=')
                cid = cid.strip()
                if cid in self.cid2uid_map:
                    cid = self.cid2uid_map[cid]
                else:
                    # if this CID is not in the INCHI file
                    self.cid2uid_map[cid] = cid
                self.compound2names_map[cid] = name.strip()
                self.cofactor_set.add(cid)
            elif command == 'SKIP':  # ignore this compound
                cid, name = line.split('=')
                cid = cid.strip()
                if cid in self.cid2uid_map:
                    cid = self.cid2uid_map[cid]
                else:
                    # if this CID is not in the INCHI file
                    self.cid2uid_map[cid] = cid
                self.compound2names_map[cid] = name.strip()
                self.ignored_set.add(cid)
            else:
                raise KeggParseException(
                    "Unknown command in Database Update file: " + command)
    
        update_file.close()

    def get_compound_name(self, cid: str) -> str:
        return self.compound2names_map.get(cid, cid)
      
    def preprocess_database(self) -> None:
        """ 
            create a new map of RID to reactants, without the co-factors.
            if the reaction is not balanced, skip it and don't add it to the new map.
        """
        unique_reactions = {}
        unique_reaction_names = set()
        for r in self.reactions:
            if r.rid in self.banned_reactions:
                logging.debug(f"This reaction has been banned: {r.name}")
            elif not r.is_futile():
                logging.debug(f"This reaction does nothing: {r.name}")
            elif not r.is_balanced(self.compound2atoms_map):
                logging.debug(f"This reaction isn't balanced: {r.name}")
            elif len(self.banned_compounds.intersection(r.get_cids())) > 0:
                logging.debug(f"This reaction contains a banned compound: "
                              f"{r.name}")
            else:
                s = r.unique_string()
                if s not in unique_reactions:
                    unique_reactions[s] = r
                    
                if r.name in unique_reaction_names:
                    logging.debug(
                        f"Warning: Duplicate reaction {r.name} appears with "
                        f"different formulae")
                else:
                    unique_reaction_names.add(r.name)

        self.reactions = list(unique_reactions.values())

    def add_reaction(
            self,
            rid: str,
            direction: str,
            sparse_reaction: Dict[str, float],
            name: str = None,
            weight: float = 1.0
    ) -> None:
        """Add another reaction to the universal matrix

        :param rid: the reaction ID
        :param direction: the direction (=>, <=>, or <=)
        :param sparse_reaction: a dictionary representing the reaction
        :param name: the name of the reaction
        :param weight: the weight to use when counting path length
        """
        if name is None:
            name = rid
       
        if direction in ["=>", "<=>"]:
            self.reactions.append(
                Reaction(name + "_F", rid, sparse_reaction, weight)
            )
        if direction in ["<=", "<=>"]:
            self.reactions.append(
                Reaction(name + "_R", rid,
                         Kegg.reverse_sparse_reaction(sparse_reaction), weight)
            )
    
    @staticmethod
    def reverse_sparse_reaction(sparse_reaction):
        backward_reaction = {}
        for cid, coeff in sparse_reaction.items():
            backward_reaction[cid] = -coeff
        return backward_reaction
    
    def get_stoichiometric_matrix(self) -> Tuple[pd.Series, pd.DataFrame]:
        """create the stoichiometric matrix from the list of reactions.

        :return: (f, stoich_df) - f is a Series with the optimization
        function coeffs, and stoichiometry is the DataFrame containing the
        stoichiometric matrix in sparse form.
        """
        compounds = set()
        for r in self.reactions:
            compounds = compounds.union(r.get_cids())
        compounds = compounds.difference(self.ignored_set)
        compounds = list(sorted(compounds))

        # create the stoichiometric matrix
        logging.info(f"Universal matrix comprises {len(self.reactions)} "
                     f"reactions with {len(compounds)} unique compounds")

        stoich_data = []
        for rxn in self.reactions:
            for cid, coeff in rxn.sparse_reaction.items():
                if cid in compounds and coeff != 0:
                    stoich_data.append((cid, rxn, coeff))

        stoich_df = pd.DataFrame(
            stoich_data, columns=["compound", "reaction", "coefficient"])

        opt_func = pd.Series(index=self.reactions,
                             data=[r.weight for r in self.reactions],
                             name="weight")

        return opt_func, stoich_df
    
    def create_compound_node(self, cid: str) -> pydot.Node:
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set_label(f'"{self.get_compound_name(cid)}"')
        node.set_tooltip(cid)
        node.set_fontsize("12")
        node.set_fontname(self.font)
        node.set_style("filled")

        if cid[0] != "C":
            node.set_shape("tripleoctagon")
            node.set_fontcolor("darkgreen") # color for environment
            node.set_fillcolor("white")
        elif cid in self.cofactor_set:
            node.set_shape("plaintext")
            node.set_URL(f'"https://www.kegg.jp/dbget-bin/www_bget?cpd:{cid}"')
            node.set_fontcolor("dodgerblue") # color for cofactors
            node.set_fillcolor("white")
        else:
            node.set_shape("octagon")
            node.set_URL(f'"https://www.kegg.jp/dbget-bin/www_bget?cpd:{cid}"')
            node.set_fontcolor("white") # color for non-cofcators
            node.set_fillcolor("dodgerblue")
        return node

    def create_reaction_node(self, rxn: Reaction) -> pydot.Node:
        node = pydot.Node(name=str(uuid.uuid1()))
        node.set_label(f'"{rxn.name}"')
        node.set_tooltip(rxn.rid)
        node.set_URL(f'"https://www.kegg.jp/dbget-bin/www_bget?rn:{rxn.rid}"')
        node.set_shape("oval")
        node.set_style("filled")
        node.set_fontcolor("darkgreen")
        node.set_fillcolor("white")
        node.set_fontsize("7")
        node.set_fontname(self.font)
        return node

    def create_edge(
            self,
            c_node: pydot.Node,
            r_node: pydot.Node,
            coeff: float = 1.0
    ) -> pydot.Edge:

        if coeff < 0:
            edge = pydot.Edge(c_node, r_node)
        else:
            edge = pydot.Edge(r_node, c_node)

        if abs(abs(coeff) - 1.0) > 1e-3:
            edge.set_label(f'"{abs(coeff):.3g}"')

        edge.set_arrowhead("open")
        edge.set_arrowtail("none")
        edge.set_color("cadetblue")  # edge line color
        edge.set_fontcolor("indigo")  # edge label color
        edge.set_fontname(self.font)
        edge.set_fontsize("10")
        return edge

    def draw_pathway(
            self,
            stoich_df: pd.DataFrame,
            solution: pd.Series
    ) -> pydot.Dot:
        """Draw a pathway as a graphviz image."""
        Gdot = pydot.Dot()
        df = stoich_df.loc[stoich_df.reaction.isin(solution.index)]

        r_nodes: Dict[Reaction, pydot.Node] = {}  # dictionary of reaction nodes
        for reaction, flux in solution.items():
            node = self.create_reaction_node(reaction)
            r_nodes[reaction] = node
            Gdot.add_node(node)

        for compound in df["compound"].unique():
            if compound in self.ignored_set:
                continue
            elif compound in self.cofactor_set:
                # make a special node of this co-factor for each reaction
                # that uses it
                for row in df[df["compound"] == compound].itertuples():
                    node = self.create_compound_node(compound)
                    Gdot.add_node(node)

                    edge = self.create_edge(
                            node, r_nodes[row.reaction],
                            row.coefficient * solution[row.reaction]
                    )
                    Gdot.add_edge(edge)
            else:
                # This is a normal metabolite, create only one node for it
                node = self.create_compound_node(compound)
                Gdot.add_node(node)
                for row in df[df["compound"] == compound].itertuples():
                    edge = self.create_edge(
                            node, r_nodes[row.reaction],
                            row.coefficient * solution[row.reaction]
                    )
                    Gdot.add_edge(edge)

        return Gdot
