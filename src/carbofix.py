from typing import List, TextIO, Union
import numpy as np
import json
import pandas as pd
from optlang import (Model, Variable, Constraint, Objective, symbolics)
import logging
import os
from os import path

from . import RES_PATH, REC_PATH
from .kegg import Kegg, Reaction

if not path.exists(RES_PATH):
    os.mkdir(RES_PATH)


class Carbofix(object):
    """Finds pathways using the Universal stoichiometric matrix."""

    def __init__(self, update_file: str = None):
        if update_file is None:
            self.UPDATE_FILE = path.join(REC_PATH, "database_updates.txt")
        else:
            self.UPDATE_FILE = update_file
        self.ENVIRONMENT = "__environment__"
        self.BIOMASS = "__biomass__"
        self.UPPER_BOUND = 100.0

        self.kegg = Kegg()
        self.kegg.prepare_all(self.UPDATE_FILE)
        self.kegg.add_reaction("R00000", '=>', {self.ENVIRONMENT: 1},
                               name='IMPORT', weight=0.0)

    def find_path(
            self,
            experiment_name: str,
            milp_factor: float = 0.0
    ) -> None:
        """

        :param experiment_name:
        :param sources:
        :param targets:
        :param milp_factor: the factor by which to multiply the minimal path
        length in order to get the maximal considered path length
        :return:
        """
        opt_func, stoich_df = self.kegg.get_stoichiometric_matrix()

        # Find a solution with a minimal total flux
        solution = self.linprog(opt_func, stoich_df)
        if solution is None:
            logging.warning("Couldn't find any cycle")
        else:
            # Use iterative MILP to find the best suboptimal solutions
            min_flux = solution.sum()
            previous_solutions = []
            counter = 0
            sol_fname = path.join(RES_PATH, f"{experiment_name}_solutions.txt")
            with open(sol_fname, "wt") as solution_file:
                while solution.sum() <= min_flux * milp_factor:

                    self.write_solution(
                        solution_file, counter, stoich_df, solution
                    )

                    logging.info(
                        f"Found a minimal cycle with {len(solution)} unique "
                        f"reactions and a total flux of {solution.sum():.3g}")

                    Gdot = self.kegg.draw_pathway(stoich_df, solution)
                    Gdot.write_svg(
                        path.join(RES_PATH,
                                  f"{experiment_name}_{counter:03d}.svg"),
                        prog="dot"
                    )

                    # create the MILP problem to constrain the previous 
                    # solutions not to reappear again.
                    previous_solutions.append(solution)
                    solution = self.linprog(
                        opt_func, stoich_df, previous_solutions
                    )
                    if solution is None:
                        logging.info("Couldn't find any more cycles")
                        break

                    counter += 1

    def add_source(self, formula: str) -> None:
        sparse_reaction = Reaction.parse_reaction_formula_side(formula)
        sparse_reaction[self.ENVIRONMENT] = -1
        self.kegg.add_reaction("R99999", "=>", sparse_reaction, name="IMPORT",
                               weight=0.0)

    def add_sink(self, formula: str) -> None:
        sparse_reaction = Reaction.parse_reaction_formula_side(formula)
        sparse_reaction[self.BIOMASS] = -1
        self.kegg.add_reaction("R99998", "<=", sparse_reaction, name="EXPORT",
                               weight=0.0)

    def add_import_and_export(
            self,
            sources: List[str],
            targets: List[str]
    ) -> None:
        for formula in sources:
            self.add_source(formula)

        for formula in targets:
            self.add_sink(formula)

    def linprog(
            self,
            opt_func: pd.Series,
            stoich_df: pd.DataFrame,
            previous_solutions: List[pd.Series] = None,
            lp_path: str = None
    ) -> Union[pd.Series, None]:
        """Create a Linear Problem for finding a minimal-length pathway.

        :param f: Series containing the optimization goal vector
        :param stoich_df: DataFrame containing the stoichiometric matrix
        :param previous_solutions: list of previous solutions (to exclude)
        :param lp_path: if not None, save the LPs to this file (for debugging)
        :return: A solutions as a pandas Series, or None if there are none
        """
        model = Model(name="carbofix")
        logging.debug(
            f"Creating path-finding LP using {model.interface.__name__}")

        reactions = stoich_df.reaction.unique()

        # create the continuous flux variables
        flux_vars = pd.Series(
            index=reactions,
            data=[Variable(f"v_{rxn.name}_{i:05d}", lb=0,
                           ub=self.UPPER_BOUND, type="continuous")
                  for i, rxn in enumerate(reactions)],
            name="flux"
        )
        model.add(flux_vars)

        # create the boolean indicator variables for all reaction fluxes
        indicator_vars = pd.Series(
            index=reactions,
            data=[Variable(f"g_{rxn.name}_{i:05d}", type="binary")
                  for i, rxn in enumerate(reactions)],
            name="indicator"
        )
        model.add(indicator_vars)

        constraints = []

        # Make each gamma_r into a flux indicator
        # (i.e. so that if v_r > 0 then gamma_i must be equal to 1).
        # We use the following constraint:
        #          - M - 1 <= v_i - M * gamma_i <= 0
        logging.debug("Adding constraints: flux-indicators")
        for i, rxn in enumerate(reactions):
            constraints.append(
                Constraint(
                    flux_vars[rxn] - self.UPPER_BOUND * indicator_vars[rxn],
                    lb=-self.UPPER_BOUND - 1, ub=0,
                    name=f"constr_{rxn.name}_{i:05d}")
            )

        df = stoich_df.join(flux_vars, on="reaction")  # name = flux
        df = df.join(indicator_vars, on="reaction")  # name = indicator
        df["flux_times_coeff"] = df.flux.multiply(df.coefficient)

        logging.debug(f"Adding constraints: mass balance")
        for compound, group_df in df.groupby("compound"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            bounds = (1, 1) if compound == self.BIOMASS else (0, 0)
            constraints.append(
                Constraint(expr, lb=bounds[0], ub=bounds[1],
                           name=f"mass_balance_{compound}"))

        if previous_solutions is not None:
            # for each previous solution, add constraints on the indicator
            # variables, so that that solution will not repeat (i.e. the sum
            # over the previous reaction set must be less than the size of
            # that set).
            logging.debug("Adding constraints: exclude previous solutions")
            for i, previous_solution in enumerate(previous_solutions):
                sum_of_gammas = symbolics.add(
                    indicator_vars[previous_solution.index])
                constraints.append(
                    Constraint(sum_of_gammas, lb=0,
                               ub=previous_solution.shape[0] - 1,
                               name=f"exclude_solution_{i:05d}")
                )

        model.add(constraints)

        logging.debug(f"Adding objective: minimums sum of all indicators")
        model.objective = Objective(
           symbolics.Zero, direction='min', name="sum_of_gammas"
        )
        model.objective.set_linear_coefficients(
           {indicator_vars[rxn]: float(opt_func[rxn]) for rxn in reactions}
        )
        if lp_path is not None:
            fname = path.join(RES_PATH, f"{lp_path}.json")
            logging.debug("Writing LP to {fname}")
            with open(fname, "wt") as fp:
                json.dump(model.to_json(), fp, indent="    ")

        logging.debug(f"Optimizing MILP")
        if model.optimize() != "optimal":
            return None

        # make a sparse representation of the solution flux vector
        active_reactions = {rxn: var.primal
                            for rxn, var in flux_vars.items()
                            if abs(var.primal) > 1e-3}

        solution = pd.Series(data=active_reactions)
        return solution

    def write_solution(
            self,
            handle: TextIO,
            counter: int,
            stoich_df: pd.DataFrame,
            solution: pd.Series
    ) -> None:

        def write_compound_and_coeff(compound: str, coeff: float) -> str:
            """Write a single reactant and its coefficient."""
            if np.abs(coeff - 1.0) < 1e-6:
                return f"{compound}"
            else:
                return f"{coeff} {compound}"

        def write_reaction(stoich_df: pd.DataFrame, rxn: Reaction) -> str:
            """Write a reaction forumla to a string."""
            rxn_df = stoich_df[stoich_df.reaction == rxn]
            rxn_df = rxn_df[["compound", "coefficient"]].set_index("compound")
            left = []
            right = []
            for compound, coeff in rxn_df.coefficient.items():
                if coeff < 0:
                    left.append(write_compound_and_coeff(compound, -coeff))
                else:
                    right.append(write_compound_and_coeff(compound, coeff))

            return " + ".join(left) + " => " + " + ".join(right)

        handle.write(f'ENTRY       M-PATHWAY_{counter+1:03d}\n')
        handle.write(f'SKIP        FALSE\n')
        handle.write(f'NAME        M-PATHWAY_{counter+1:03d}\n')
        handle.write(f'TYPE        MARGIN\n')
        handle.write(f'CONDITIONS  pH=7.0,I=0.0,T=300\n')
        handle.write(f'C_MID       0.0001\n')

        first = True
        for rxn, flux in solution.items():
            if first:
                prefix = "REACTION"
                first = False
            else:
                prefix = ""
            formula = write_reaction(stoich_df, rxn)
            handle.write(f"{prefix:12s}{rxn.name}   {formula} (x{flux:.3f})\n")

        handle.write('///\n')
        handle.flush()
