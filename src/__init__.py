import os

relpath = lambda s: os.path.join(
    os.path.abspath(os.path.dirname(__file__)), f"../{s}")

KEGG_PATH = relpath("kegg")
REC_PATH = relpath("rec")
RES_PATH = relpath("res")

if not os.path.exists(RES_PATH):
    os.mkdir(RES_PATH)
