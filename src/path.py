# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
"""A script for finding all possible pathways from A to B."""
import json
import logging
import re
from os import path
from typing import Dict, Iterable, Set, Union

import pandas as pd

import cobra
from cobra.test import create_test_model
from optlang import Constraint, Model, Objective, Variable, symbolics
from sbtab.SBtab import SBtabDocument

from . import RES_PATH
from .solution import Solution

# TODO: use PdfPages to write a single PDF file with all suggested pathways
# TODO: cluster similar pathway solutions (e.g. glc -> fru -> f6p and glc ->
#  g6p -> f6p)


class Path(object):
    """Finds pathways using the Universal stoichiometric matrix."""

    def __init__(
            self,
            cobra_model: cobra.Model = None
    ):
        """Create a Path for designing pathways based on goal stoichiometry.

        :param cobra_model: a COBRA model, from which to take the list of
        reactions.
        """

        # we need to have all reactions irreversible in order to create the
        # binary reaction indicators later.
        if cobra_model is None:
            self.cobra_model = create_test_model("textbook")
            self.cobra_model.reactions.ATPM.bounds = (0.0, 0.0)
            self.cobra_model.reactions.EX_glc__D_e.bounds = (0.0, 1000.0)
        else:
            self.cobra_model = cobra_model.copy()

        self.m_substrates = cobra.Metabolite(id="__substrates__")
        self.m_products = cobra.Metabolite(id="__products__")
        self.cobra_model.add_metabolites([self.m_substrates, self.m_products])

        r_import = cobra.Reaction(id="__import__")
        r_import.add_metabolites(
            {self.m_substrates: 1}
        )
        self.cobra_model.add_reaction(r_import)

        r_export = cobra.Reaction(
            id="__export__", lower_bound=1.0, upper_bound=1.0)
        r_export.add_metabolites(
            {self.m_products: -1}
        )
        self.cobra_model.add_reaction(r_export)

        self.cofactor_set: Set[cobra.Metabolite] = set()

        self.lp_model = None

    @property
    def metabolites(self) -> Iterable[cobra.Metabolite]:
        """Return the list of COBRA metabolites in the model."""
        return self.cobra_model.metabolites

    @property
    def reactions(self) -> Iterable[cobra.Reaction]:
        """Return the list of COBRA reactions in the model."""

        # First check for duplicate reactions, i.e. reactions that are
        # identical in terms of stoichiometry
        rxn_hashes = []
        for rxn in self.cobra_model.reactions:
            rxn_hashes.append((rxn, str(rxn.metabolites)))
        df = pd.DataFrame(data=rxn_hashes,
                          columns=["reaction", "hash"])
        unique_df = df.drop_duplicates(subset="hash", keep="first")

        return unique_df.reaction

    @property
    def stoichiometry_df(self) -> pd.DataFrame:
        """Convert the stoichiometric data to a DataFrame.

        :return: a DataFrame containing the sparse stoichiometric matrix.
        """
        stoichiometric_data = []
        for rxn in self.reactions:
            for met in rxn.metabolites:
                stoichiometric_data.append((rxn, met, met.id,
                                            rxn.get_coefficient(met)))
        return pd.DataFrame(
            stoichiometric_data, columns=["reaction", "metabolite",
                                          "metabolite_id", "coefficient"])

    def add_reaction(
            self,
            id: str,
            metabolites_to_add: Dict[Union[cobra.Metabolite, str], float],
            lower_bound: int = -1000,
            upper_bound: int = 1000
    ):
        """Insert (or override) a reaction to the COBRA model."""
        if self.cobra_model.reactions.has_id(id):
            self.cobra_model.reactions.get_by_id(id).remove_from_model()
        rxn = cobra.Reaction(id=id, lower_bound=lower_bound,
                             upper_bound=upper_bound)
        self.cobra_model.add_reaction(rxn)
        rxn.add_metabolites(metabolites_to_add)
        return rxn

    def set_substrates(
            self,
            metabolites_to_add: Dict[Union[cobra.Metabolite, str], float]
    ) -> None:
        """Add a dictionary of substrates to the path search problem."""
        rxn = self.add_reaction("__substrates_import__", metabolites_to_add,
                                lower_bound=0, upper_bound=1000)
        rxn.add_metabolites({self.m_substrates: -1})

    def set_products(
            self,
            metabolites_to_add: Dict[Union[cobra.Metabolite, str], float]
    ) -> None:
        """Add a dictionary of products to the path search problem."""
        rxn = self.add_reaction("__products_export__", metabolites_to_add,
                                lower_bound=-1000, upper_bound=0)
        rxn.add_metabolites({self.m_products: -1})

    def merge_metabolites(
            self,
            new_metabolite_name: str,
            old_metabolites: Iterable[Union[cobra.Metabolite, str]]
    ):
        """Merge a list of metabolites into one. This helps in deduping
        reactions later."""

        group_met = cobra.Metabolite(id=new_metabolite_name)
        self.cobra_model.add_metabolites(group_met)
        for met in old_metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            for rxn in met.reactions:
                rxn.add_metabolites({group_met: rxn.get_coefficient(met)})
            self.cobra_model.remove_metabolites(met)

    def remove_metabolites(
            self,
            metabolites: Iterable[Union[cobra.Metabolite, str]]
    ) -> None:
        """Remove metabolites from the model."""
        for met in metabolites:
            if type(met) == str:
                met = self.cobra_model.metabolites.get_by_id(met)
            self.cobra_model.remove_metabolites(met)

    def add_cofactors(
            self,
            cofactors: Iterable[Union[cobra.Metabolite, str]]
    ) -> None:
        """Add metabolites to the set of co-factors."""
        for c in cofactors:
            if type(c) == str:
                c = self.cobra_model.metabolites.get_by_id(c)
            self.cofactor_set.add(c)

    def create_lp(self) -> None:
        """Create the LP for finding the path with min number of reactions."""
        self.lp_model = Model(name="path-designer")
        logging.debug(
            f"Creating path-finding LP using solver: "
            f"{self.lp_model.interface.__name__}")

        irreversible_reaction = []
        for rxn in self.reactions:
            if rxn.upper_bound > 0:
                irreversible_reaction.append(
                    (rxn, rxn.id + "_F", 1,
                     max(rxn.lower_bound, 0), rxn.upper_bound)
                )
            if rxn.lower_bound < 0:
                irreversible_reaction.append(
                    (rxn, rxn.id + "_R", -1,
                     max(-rxn.upper_bound, 0), -rxn.lower_bound)
                )
        irreversible_reaction_df = pd.DataFrame(
            data=irreversible_reaction,
            columns=["reaction", "irreversible_reaction_id", "direction",
                     "lower_bound", "upper_bound"]
        )
        logging.debug("Creating flux variables")
        irreversible_reaction_df["flux"] = \
            [Variable(f"v_{row.irreversible_reaction_id}",
                                        lb=row.lower_bound,
                                        ub=row.upper_bound,
                                        type="continuous")
             for row in irreversible_reaction_df.itertuples()]

        logging.debug("Creating flux indicators")
        gammas = []
        indicator_constraints = []
        for row in irreversible_reaction_df.itertuples():
            if row.irreversible_reaction_id[0:2] != "__":
                # only add indicators to reactions which are in the original
                # model (not ones added by path-designer such as import,
                # export, and co-factor regeneration
                gamma = Variable(f"g_{row.irreversible_reaction_id}",
                                 type="binary")
                gammas.append(gamma)

                # Make each gamma_r into a flux indicator
                # (i.e. so that if v_r > 0 then gamma_i must be equal to 1).
                # We use the following constraint:
                #          - M - 1 <= v_i - M * gamma_i <= 0
                indicator_constraints.append(
                    Constraint(row.flux - row.upper_bound * gamma,
                               lb=(-row.upper_bound - 1.0),
                               ub=0.0,
                               name=f"constr_{row.irreversible_reaction_id}")
                )
        self.lp_model.add(indicator_constraints)

        logging.debug(f"Adding minimum sum of all indicators objective")
        sum_of_gammas = symbolics.add(gammas)
        self.lp_model.objective = Objective(sum_of_gammas, direction='min',
                                            name="sum_of_gammas")

        # add stoichiometry information to each irreversible reaction
        df = irreversible_reaction_df.merge(
            self.stoichiometry_df, on="reaction")

        # adjust the sign of coefficients to the new "irreversible" directions
        df.coefficient = df.coefficient.multiply(df.direction)

        # add the flux variables to the DataFrame
        df["flux_times_coeff"] = df.flux.multiply(df.coefficient)

        logging.debug(f"Adding mass balance constraints")
        mass_balance_constraints = []
        for met_id, group_df in df.groupby("metabolite_id"):
            if group_df.shape[0] == 1:
                expr = group_df.flux_times_coeff.iat[0]
            else:
                expr = symbolics.add(group_df.flux_times_coeff)
            mass_balance_constraints.append(
                Constraint(expr, lb=0.0, ub=0.0,
                           name=f"mass_balance_{met_id}"))

        self.lp_model.add(mass_balance_constraints)

    def exclude_solution(self, solution: Solution, counter: int = 0) -> None:
        """Exclude a solution from the LP."""

        # for each previous solution, add constraints on the indicator
        # variables, so that that solution will not repeat (i.e. the sum
        # over the previous reaction set must be less than the size of
        # that set).
        logging.debug("Adding constraints: exclude previous solutions")
        self.lp_model.add(Constraint(solution.sum_of_gammas, lb=0,
                          ub=solution.objective_value - 1,
                          name=f"exclude_solution_{counter:03d}"))

    def write_lp(self, lp_path):
        """write the LP to this file (only for debugging)."""
        logging.debug(f"Writing LP to {lp_path}")
        with open(lp_path, "wt") as fp:
            json.dump(self.lp_model.to_json(), fp, indent="    ")

    def get_next_solution(self) -> Union[Solution, None]:
        """Solve the LP and find the next shortest path.

        :return: A solution or None
        """

        logging.debug(f"Optimizing MILP")
        if self.lp_model.optimize() != "optimal":
            return None

        # make a sparse representation of the solution flux vector
        active_indicators = []
        fluxes = {}
        for var_name, var in self.lp_model.variables.items():
            if var.type == "binary" and var.primal:
                active_indicators.append(var)
            elif abs(var.primal) > 1e-3:
                rxn_id, direction = re.findall(r"v_(\w+)_([FR])", var_name)[0]
                if direction == "F":
                    fluxes[rxn_id] = var.primal
                else:
                    fluxes[rxn_id] = -var.primal

        solution = Solution(
            self,
            self.lp_model.objective.value,
            active_indicators,
            pd.Series(fluxes)
        )

        return solution

    def find_paths(
            self,
            experiment_name: str,
            milp_factor: float = 0.0,
            max_iterations: int = 1000,
            write_lp: bool = False
    ) -> None:
        """Find all the n-shortest paths that satisfy the constraints.

        :param experiment_name:
        :param milp_factor: the factor by which to multiply the minimal path
        length in order to get the maximal considered path length
        :param max_iterations: maximum number of iterations
        :param write_lp: whether to write each of the LPs to files
        :return:
        """

        max_objective = None
        self.create_lp()

        sbtab_filename = path.join(RES_PATH, f"{experiment_name}.tsv")
        sbtabdoc = SBtabDocument('pathways', filename=sbtab_filename)

        # Use iterative MILP to find the n-best optimal solutions
        for counter in range(max_iterations):
            if write_lp:
                lp_path = path.join(RES_PATH,
                                    f"{experiment_name}_{counter:03d}.lp")
                self.write_lp(lp_path)

            solution = self.get_next_solution()

            if solution is None:
                logging.warning("Couldn't find any more solutions")
                break
            logging.info(
                f"Found path with {solution.objective_value:.3g} "
                f"reactions (total flux = {solution.total_flux:.3g})"
            )

            sbtabdoc.add_sbtab(solution.to_sbtab(counter))
            sbtabdoc.write()

            g_dot = solution.draw_graph()
            g_dot.write_svg(
                path.join(RES_PATH,
                          f"{experiment_name}_{counter:03d}.svg"),
                prog="dot"
            )

            if milp_factor > 0:
                if max_objective is None:
                    max_objective = solution.objective_value * milp_factor
                elif solution.objective_value > max_objective:
                    break

            self.exclude_solution(solution, counter)
