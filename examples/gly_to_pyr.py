import logging
from src.carbofix import Carbofix

logging.getLogger().setLevel(logging.INFO)
cf = Carbofix(update_file="../rec/database_updates.txt")

cf.add_source("C00116 + 2 C00003")
cf.add_sink("C00022 + 2 C00004")
cf.find_path('gly_to_pyr', milp_factor=1.5)
