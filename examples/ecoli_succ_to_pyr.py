import logging
from src.path import Path
from cobra import Reaction
from cobra.test import create_test_model

logging.getLogger().setLevel(logging.INFO)

cobra_model = create_test_model("ecoli")
cobra_model.reactions.ATPM.bounds = (0.0, 0.0)
cobra_model.reactions.EX_glc_e.bounds = (0.0, 0.0)

cf = Path(cobra_model)
cf.set_substrates({"succ_c": 1})
cf.set_products({"pyr_c": 1})

cf.merge_metabolites("e_acceptor",
                     ["nad_c", "nadp_c", "q8_c", "mqn8_c", "trdox_c"])
cf.merge_metabolites("e_donor",
                     ["nadh_c", "nadph_c", "q8h2_c", "mql8_c", "trdrd_c"])
cf.merge_metabolites("NTP", ["atp_c", "gtp_c", "ctp_c"])
cf.merge_metabolites("NDP", ["adp_c", "gdp_c", "cdp_c"])
cf.merge_metabolites("NMP", ["amp_c", "gmp_c", "cmp_c"])
cf.remove_metabolites(["h2o_c", "h_c", "h_e", "h_p"])

cf.add_reaction("__regenerate_NTP_", {"NDP": -1, "NTP": 1})
cf.add_reaction("__regenerate_electron_carrier__",
                {"e_acceptor": -1, "e_donor": 1})
for free_met in ["co2_c", "o2_c", "pi_c"]:
    cf.add_reaction(f"__regenerate_{free_met}__", {free_met: 1})

cf.add_cofactors(["NDP", "NTP", "e_acceptor", "e_donor",
                  "co2_c", "o2_c", "pi_c"])

cf.find_paths('succ_to_pyr', milp_factor=0, max_iterations=4)
