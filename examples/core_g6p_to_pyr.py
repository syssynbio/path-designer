import logging
from src.path import Path
from cobra.test import create_test_model

logging.getLogger().setLevel(logging.INFO)

cobra_model = create_test_model("textbook")
cobra_model.reactions.ATPM.bounds = (0.0, 0.0)
cobra_model.reactions.EX_glc__D_e.bounds = (0.0, 0.0)

cf = Path(cobra_model)
cf.set_substrates({"g6p_c": 1})
cf.set_products({"pyr_c": 1})

cf.merge_metabolites("NAD(P)", ["nad_c", "nadp_c"])
cf.merge_metabolites("NAD(P)H", ["nadh_c", "nadph_c"])
cf.add_reaction("__regenerate_ATP__", {"adp_c": -1, "atp_c": 1})
cf.add_reaction("__regenerate_NAD__", {"NAD(P)": -1, "NAD(P)H": 1})
cf.remove_metabolites(["h2o_c", "h_c", "h_e"])
for free_met in ["pi_c", "co2_c", "o2_c"]:
    cf.add_reaction(f"__regenerate_{free_met}__", {free_met: 1})

cf.add_cofactors(["adp_c", "atp_c", "NAD(P)", "NAD(P)H",
                  "co2_c", "o2_c", "pi_c"])
cf.find_paths('g6p_to_pyr', milp_factor=0, max_iterations=4)
