import logging
from src.path import Path
from cobra import Reaction
from cobra.test import create_test_model

logging.getLogger().setLevel(logging.INFO)

cobra_model = create_test_model("ecoli")
cobra_model.reactions.ATPM.bounds = (0.0, 0.0)
cobra_model.reactions.ATPS4rpp.bounds = (0.0, 0.0)
cobra_model.reactions.EX_glc_e.bounds = (0.0, 0.0)

# r_atp = Reaction(id="__regenerate_ATP__", lower_bound=-1000, upper_bound=1000)
# cobra_model.add_reaction(r_atp)
# r_atp.add_metabolites({"adp_c": -1, "atp_c": 1})

for n_atp in [-2, -1, 0, 1, 2]:
    logging.info(f"Looking for pathways from glucose to pyruvate that make "
                 f"{n_atp} ATPs")

    cf = Path(cobra_model)
    cf.set_substrates({"glc__D_c": 1, "adp_c": n_atp, "pi_c": n_atp})
    cf.set_products({"pyr_c": 2, "atp_c": n_atp})

    cf.merge_metabolites("e_acceptor",
                         ["nad_c", "nadp_c", "q8_c", "mqn8_c", "trdox_c"])
    cf.merge_metabolites("e_donor",
                         ["nadh_c", "nadph_c", "q8h2_c", "mql8_c", "trdrd_c"])

    cf.add_reaction("__regenerate_electron_carrier__",
                    {"e_acceptor": -1, "e_donor": 1})
    cf.remove_metabolites(["h2o_c", "h_c", "h_e", "h_p"])
    for free_met in ["pi_c", "co2_c", "o2_c"]:
        cf.add_reaction(f"__regenerate_{free_met}__", {free_met: 1})

    cf.add_cofactors(["adp_c", "atp_c", "e_acceptor", "e_donor",
                      "co2_c", "o2_c", "pi_c"])
    cf.find_paths(f"glc_to_pyr_{n_atp}atp", milp_factor=0, max_iterations=5,
                  write_lp=False)
