import logging
from src.carbofix import Carbofix
from os import path

REC_PATH = path.join(path.abspath(path.dirname(__file__)), "../rec")

logging.getLogger().setLevel(logging.DEBUG)
cf = Carbofix(update_file=path.join(REC_PATH, "database_updates.txt"))
cf.add_source("C00031 + C00006")
cf.add_sink("C00345 + C00005")
cf.find_path('glc_to_6pg', milp_factor=1.5)
