import logging
from src.carbofix import Carbofix

logging.getLogger().setLevel(logging.DEBUG)
cf = Carbofix(update_file="../rec/database_updates.txt")

cf.add_source("C00011")
cf.add_sink("C00048")
cf.find_path('co2_to_glyoxylate', milp_factor=1.5)
