# carbofix
A algorithm for automatic design of carbon fixation cycles

Requirements:
- CPLEX (from IBM)
- python 3.7
- pydot, numpy, scipy, pandas
- optlang
- recommended: symengine, jupyterlab

Example:
```
python -m examples.glc_to_g6p
```
(see results in /res)
